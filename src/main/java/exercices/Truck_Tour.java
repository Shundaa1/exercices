package exercices;

import java.io.*;
import java.util.*;


public class Truck_Tour {
	public static int n;
    static int truckTour(int[][] petrolpumps) {
    	int menorIndex=n,fuel=0;
    	for(int i=0;i<n;i++) {
    		fuel=0;
        	fuel=petrolpumps[i][0]-petrolpumps[i][1];
        	if(fuel<0)
        		continue;
    	if(fuel>=0&&i<menorIndex)
    		menorIndex=i;
    	}
    	return menorIndex;
    }  
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        n = Integer.parseInt(scanner.nextLine().trim());

        int[][] petrolpumps = new int[n][2];

        for (int petrolpumpsRowItr = 0; petrolpumpsRowItr < n; petrolpumpsRowItr++) {
            String[] petrolpumpsRowItems = scanner.nextLine().split(" ");

            for (int petrolpumpsColumnItr = 0; petrolpumpsColumnItr < 2; petrolpumpsColumnItr++) {
                int petrolpumpsItem = Integer.parseInt(petrolpumpsRowItems[petrolpumpsColumnItr].trim());
                petrolpumps[petrolpumpsRowItr][petrolpumpsColumnItr] = petrolpumpsItem;
            }
        }

        int result = truckTour(petrolpumps);

        System.out.println(result);
    }
}
