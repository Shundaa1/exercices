package exercices;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
public class Stock_Maximize {
	
    public static long stockmax(List<Integer> prices) {
    	
    	int money=0,stock=0,priceMax=0;
    	boolean isPriceMax=true;
		priceMax=0;
    	for(int i=0;i<prices.size();i++){
    		isPriceMax=true;
    		for(int j=i+1;j<prices.size();j++) {
    			if(prices.get(j)>priceMax) {
					priceMax=prices.get(j);
    				if(isPriceMax) {
        				money-=prices.get(i);
        				isPriceMax=false;
    				}
    			}
    		}
			money=money + priceMax;
			priceMax=0;
    	}
		return money;
    }
    @Test
    public void testQuery()  {
    	//List<Integer> list = Arrays.asList(1,2,100);
    	List<Integer> list = Arrays.asList(1,3,1,2); 
    	//assertTrue(stockmax(list)==197);
    	System.out.println(stockmax(list));
    }
}

