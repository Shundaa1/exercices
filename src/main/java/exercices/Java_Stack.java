package exercices;

import java.util.Scanner;
import java.util.Stack;

public class Java_Stack {


	public static void main(String []argh)
	{
		Scanner sc = new Scanner(System.in);
		Boolean isBalanced=true;
		Stack<Character> charStack = new Stack<Character>();
		String resposta= new String();
		String input = new String();
		while (sc.hasNext()) {
			isBalanced = true;
			input=sc.next();
			charStack.clear();
			for(int i=0;i<input.length()&&isBalanced;i++) {
				if(input.charAt(i)=='{'||input.charAt(i)=='('||input.charAt(i)=='[') 
					charStack.push(input.charAt(i));
				else if(input.charAt(i)=='}'&&!charStack.isEmpty()){
					if(charStack.pop()!='{')
						isBalanced=false;
					}
				else if(input.charAt(i)==']'&&!charStack.isEmpty()){
					if(charStack.pop()!='[')
						isBalanced=false;
					}
				else if(input.charAt(i)==')'&&!charStack.isEmpty()){
					if(charStack.pop()!='(')
						isBalanced=false;
					}
				else
					isBalanced=false;
			}
			
			if(!charStack.isEmpty()) {
				isBalanced=false;
			}
			resposta+=isBalanced.toString()+"\n";
		}
		System.out.println(resposta);
		sc.close();
	}
}

